package ua.com.user576.netsmartdoorbell

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class PrefsManager @Inject constructor(@ApplicationContext context: Context) {

    private val sharedPreferences = context.getSharedPreferences("esp32_address", Context.MODE_PRIVATE)

    fun setEsp32Address(newAddress: String) {
        sharedPreferences.edit()
            .putString(IP_ADDRESS_KEY, newAddress)
            .apply()
    }

    fun getEsp32Address() = sharedPreferences.getString(IP_ADDRESS_KEY, NET_MASK)!!

    private companion object {
        const val IP_ADDRESS_KEY = "ip_address_key"
    }
}