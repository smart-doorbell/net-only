package ua.com.user576.netsmartdoorbell

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NetDoorbellApp: Application()
