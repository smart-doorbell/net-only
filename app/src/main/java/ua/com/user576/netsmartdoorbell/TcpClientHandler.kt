package ua.com.user576.netsmartdoorbell

import android.util.Log
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.util.Scanner

class TcpClientHandler(private val dataInputStream: DataInputStream, private val dataOutputStream: DataOutputStream) : Thread() {
    override fun run() {
        while (true) {
            try {
                if (dataInputStream.available() > 0){
                    val scanner = Scanner(dataInputStream)
                    val content = scanner.nextLine()
                    if (content == "Button was pressed!") {
                        Log.i(TAG, "it's time to do action!")
                        val strTimeStamp = scanner.nextLine()
                        Log.i(TAG, "strTimeStamp = $strTimeStamp")

                        RecordsManager.createAndAddRecord(strTimeStamp)
                        return
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
                try {
                    dataInputStream.close()
                    dataOutputStream.close()
                } catch (ex: IOException) {
                    ex.printStackTrace()
                }
            } catch (e: InterruptedException) {
                e.printStackTrace()
                try {
                    dataInputStream.close()
                    dataOutputStream.close()
                } catch (ex: IOException) {
                    ex.printStackTrace()
                }
            }
        }
    }

    companion object {
        private val TAG = TcpClientHandler::class.java.simpleName
    }

}