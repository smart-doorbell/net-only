package ua.com.user576.netsmartdoorbell

import java.net.URL

enum class LedState {
    GREEN, RED
}

object Esp32 {

    lateinit var ipAddress: String

    fun setLed(ledState: LedState) {
        val state = if (ledState == LedState.GREEN) "green" else "red"
        println("LED should be set to $state")
        URL("http://${ipAddress}/set-led-$state").content
    }
}