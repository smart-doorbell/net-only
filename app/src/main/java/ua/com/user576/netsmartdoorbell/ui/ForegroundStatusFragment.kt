package ua.com.user576.netsmartdoorbell.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.DialogFragment
import java.net.ConnectException
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import ua.com.user576.netsmartdoorbell.R
import ua.com.user576.netsmartdoorbell.TcpServerService
import kotlin.concurrent.thread

class ForegroundStatusFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        var isServiceActive = false

        thread {
            val socket = Socket()
            val inetAddress: InetAddress = InetAddress.getByName("localhost")
            //calling the constructor of the SocketAddress class
            val socketAddress: SocketAddress = InetSocketAddress(inetAddress, 4444)

            isServiceActive = try {
                socket.connect(socketAddress, 500)
                true
            } catch (exception : ConnectException) {
                exception.printStackTrace()
                false
            }

            socket.close()
        }.join()

        val onBtnClick : () -> Unit = if (isServiceActive) ::onStopBtnClick else ::onStartBtnClick

        return ComposeView(requireContext()).apply {
            setContent {
                ForegroundServiceStatusScreen(isServiceActive, onBtnClick)
            }
        }
    }

    private fun onStopBtnClick() {
        thread {
            TcpServerService.stopActiveService()
        }
        dismiss()
    }

    private fun onStartBtnClick() {
        requireActivity().startService(Intent(requireContext().applicationContext, TcpServerService::class.java))
        dismiss()
    }
}

@Composable
fun ForegroundServiceStatusScreen(isServiceActive : Boolean, onBtnClick : () -> Unit) {
    Column(modifier = Modifier.padding(16.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        Text(
            modifier = Modifier.padding(16.dp),
            text = stringResource(
                if (isServiceActive) R.string.is_running else R.string.is_not_running
            ),
            textAlign = TextAlign.Center,
            fontSize = 28.sp
        )
        OutlinedButton(
            modifier = Modifier
                .size(150.dp)
                .clip(CircleShape)
                .border(3.dp, if (isServiceActive) Color.Red else Color.Green, shape = CircleShape),
            onClick = onBtnClick
        ) {
            Text(text = if (isServiceActive) "Stop!" else "Start!", fontSize = 30.sp)
        }
    }
}

@Composable
@Preview
fun Preview() {
    ForegroundServiceStatusScreen(true) {

    }
}
