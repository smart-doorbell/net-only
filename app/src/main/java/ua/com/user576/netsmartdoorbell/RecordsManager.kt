package ua.com.user576.netsmartdoorbell

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

object RecordsManager {

    val timeStamps : LiveData<List<Long>>
        get() = liveData

    fun createAndAddRecord(timeStamp : String) {
        StorageManager.downloadAndSavePhoto(timeStamp)
        list.add(timeStamp.toLong())
        liveData.postValue(list.toList())
    }

    fun removeRecord(timeStamp: Long) {
        list.remove(timeStamp)
        liveData.postValue(list)
    }

    private val list : MutableList<Long> = arrayListOf()

    private fun fillInListWithTimeStampsFromStorage() {
        StorageManager.getPhotoNamesWithoutExtension().forEach { strTimeStamp ->
            list.add(strTimeStamp.toLong())
        }
    }

    private val liveData : MutableLiveData<List<Long>> by lazy {
        val liveData : MutableLiveData<List<Long>> = MutableLiveData()
        fillInListWithTimeStampsFromStorage()
        liveData.postValue(list)
        liveData
    }
}