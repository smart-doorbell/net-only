package ua.com.user576.netsmartdoorbell.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ua.com.user576.netsmartdoorbell.PhotosAdapter
import ua.com.user576.netsmartdoorbell.RecordsManager
import ua.com.user576.netsmartdoorbell.databinding.RequestsListBinding

class RequestsListFragment : Fragment() {

    private var binding : RequestsListBinding? = null
    private lateinit var photosAdapter: PhotosAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val nonNullBinding = RequestsListBinding.inflate(inflater, container, false)
        binding = nonNullBinding

        photosAdapter = PhotosAdapter(requireContext())
        with(nonNullBinding) {
            noRequestsLabel.visibility = View.GONE
            recyclerDescription.visibility = View.GONE
            photosRecycleView.adapter = photosAdapter
        }

        RecordsManager.timeStamps.observe(viewLifecycleOwner) { list ->
            updateLabels(list.isNotEmpty())
            photosAdapter.submitList(
                list.ifEmpty {
                    null
                }
            )
        }

        return nonNullBinding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun updateLabels(areRequests : Boolean) {
        binding?.let {
            if (!areRequests) {
                it.noRequestsLabel.visibility = View.VISIBLE
                it.recyclerDescription.visibility = View.GONE
            } else {
                it.noRequestsLabel.visibility = View.GONE
                it.recyclerDescription.visibility = View.VISIBLE
            }
            it.progressBar.visibility = View.GONE
        }
    }

    companion object {
//        private const val ON_CANCELLED_LOG = "onCancelled@RequstsListFragment"
    }
}