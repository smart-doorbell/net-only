package ua.com.user576.netsmartdoorbell.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.DialogFragment
import dagger.hilt.android.AndroidEntryPoint
import ua.com.user576.netsmartdoorbell.Esp32
import ua.com.user576.netsmartdoorbell.NET_MASK
import ua.com.user576.netsmartdoorbell.PrefsManager
import javax.inject.Inject

@AndroidEntryPoint
class IpAddressFragment : DialogFragment() {

    @Inject
    lateinit var prefs: PrefsManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val currentIp = prefs.getEsp32Address()

        val regex = "$NET_MASK(\\d+)".toRegex()
        val result = regex.find(currentIp)

        return ComposeView(requireContext()).apply {
            setContent {
                val ip = if (result == null) "" else result.groupValues[1]
                IpAddressScreen(currentIp = ip, onOkBtnClick = ::onOkBtnClick)
            }
        }
    }

    private fun onOkBtnClick(newAddress: String) {
        prefs.setEsp32Address(newAddress)
        Esp32.ipAddress = newAddress
        dismiss()
    }
}

@Composable
fun IpAddressScreen(currentIp : String, onOkBtnClick: (String) -> Unit) {

    val fontSize = 36.sp
    val ip = remember{mutableStateOf(currentIp)}

    Column(modifier = Modifier.padding(16.dp), horizontalAlignment = Alignment.CenterHorizontally){
        Text(text = "Esp32 IP address:", fontSize = 40.sp)
        Spacer(Modifier.height(40.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(text = NET_MASK, fontSize = fontSize)
            TextField(
                value = ip.value,
                textStyle = TextStyle(fontSize = fontSize),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                modifier = Modifier.width(100.dp),
                onValueChange = { newVal ->
                    println(newVal)
                    ip.value = newVal
                }
            )
        }
        Spacer(Modifier.height(40.dp))
        Button(
            enabled = ip.value.isNotEmpty(),
            modifier = Modifier.align(Alignment.End),
            onClick = { onOkBtnClick(NET_MASK + ip.value) }
        ) {
            Text("Ok", fontSize = fontSize, modifier = Modifier.width(100.dp), textAlign = TextAlign.Center)
        }
    }
}

@Composable
@Preview
fun IpPreview() {
    IpAddressScreen("22") {}
}
