package ua.com.user576.netsmartdoorbell

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.net.ConnectException
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketAddress
import java.util.concurrent.atomic.AtomicBoolean

class TcpServerService : Service() {

    private var serverSocket: ServerSocket? = null
    private val working = AtomicBoolean(true)

    private val runnable = Runnable {
        var socket: Socket? = null
        working.set(true)
        try {
            serverSocket = ServerSocket(PORT)
            while (working.get()) {
                if (serverSocket != null) {
                    socket = serverSocket!!.accept()
                    Log.i(TAG, "New client: $socket")
                    Log.i(TAG, "hostName: ${socket.inetAddress.hostName}")

                    val dataInputStream = DataInputStream(socket.getInputStream())
                    val dataOutputStream = DataOutputStream(socket.getOutputStream())

                    if (socket.inetAddress.hostName == "localhost") {
                        processLocalRequest(dataInputStream, dataOutputStream)
                    }

                    // Use threads for each client to communicate with them simultaneously
                    val t: Thread = TcpClientHandler(dataInputStream, dataOutputStream)
                    t.start()
                } else {
                    Log.e(TAG, "Couldn't create ServerSocket!")
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
            try {
                socket?.close()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
        }
    }

    private fun processLocalRequest(inpStr : DataInputStream, outStr : DataOutputStream) {
        val msg = inpStr.bufferedReader().readLine()
        Log.i(TAG, "msg = $msg")
        when(msg) {
            "stop" -> {
                Log.i(TAG, "stop was received!!!")
                stopForeground(STOP_FOREGROUND_REMOVE)
                working.set(false)
                serverSocket?.close()
            }
            "isActive" -> {
                val bufferedWriter = outStr.bufferedWriter()
                bufferedWriter.write("yes\n")
                bufferedWriter.flush()
            }
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startMeForeground()
        Thread(runnable).start()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        working.set(false)
    }

    private fun startMeForeground() {
        val notificationChannelId = packageName
        val channelName = "Tcp Server Background Service"
        val channel = NotificationChannel(notificationChannelId, channelName, NotificationManager.IMPORTANCE_NONE)
        channel.lightColor = Color.BLUE
        channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val manager = (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
        manager.createNotificationChannel(channel)
        val notificationBuilder = NotificationCompat.Builder(this, notificationChannelId)
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle("Tcp Server is running in background")
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .setContentIntent(PendingIntent.getActivity(
                applicationContext, 4444,
                Intent(applicationContext, MainActivity::class.java),
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT))
            .build()
        startForeground(2, notification)
    }

    companion object {
        private val TAG = TcpServerService::class.java.simpleName
        private const val PORT = 4444

        fun stopActiveService() {
            val socket = Socket("localhost", PORT)
            val bufferedWriter = socket.getOutputStream().bufferedWriter()
            bufferedWriter.write("stop\n")
            bufferedWriter.flush()
            socket.close()
        }

        fun stop() {
            val socket = Socket()
            val inetAddress: InetAddress = InetAddress.getByName("localhost")
            //calling the constructor of the SocketAddress class
            val socketAddress: SocketAddress = InetSocketAddress(inetAddress, 4444)

            try {
                socket.connect(socketAddress, 500)
            } catch (exception : ConnectException) {
                socket.close()
                return
            }

            val bufferedWriter = socket.getOutputStream().bufferedWriter()
            bufferedWriter.write("stop\n")
            bufferedWriter.flush()

            socket.close()
        }
    }
}
