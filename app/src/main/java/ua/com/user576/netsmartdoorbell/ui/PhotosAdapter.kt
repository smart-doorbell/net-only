package ua.com.user576.netsmartdoorbell

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.ListAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import ua.com.user576.netsmartdoorbell.ui.ResolveRequestFragment

object TimeStampDiffCallback : DiffUtil.ItemCallback<Long>() {
    override fun areItemsTheSame(oldItem: Long, newItem: Long): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Long, newItem: Long): Boolean {
        return oldItem == newItem
    }
}

class PhotosAdapter(private val activityContext : Context) :
    ListAdapter<Long, PhotosAdapter.DoorbellRecordViewHolder>(TimeStampDiffCallback) {

    class DoorbellRecordViewHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val tvRingTime : TextView = view.findViewById(R.id.ring_time_text_view)
        val ivPhoto : ImageView = view.findViewById(R.id.photo)
        var timeStamp : Long = 0

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            if (view == null) {
                Log.w(LOG_TAG, VIEW_IS_NULL)
            } else {
                val activity = view.context
                if (activity is MainActivity) {
                    activity.toResolveRequestFragment(
                        bundleOf(
                            ResolveRequestFragment.TIME_INSTANT_READABLE_STRING_KEY to this.tvRingTime.text.toString(),
                            ResolveRequestFragment.TIME_INSTANT_KEY to timeStamp
                        )
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoorbellRecordViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        val colorRes = when(viewType) {
            0 -> android.R.color.holo_green_light
            1 -> R.color.second_request
            else -> android.R.color.holo_red_light
        }
        view.setBackgroundColor(
            ContextCompat.getColor(activityContext, colorRes)
        )
        return DoorbellRecordViewHolder(view)
    }

    private val formatter = SimpleDateFormat(activityContext.getString(R.string.format_pattern), Locale.getDefault())

    override fun onBindViewHolder(holder: DoorbellRecordViewHolder, position: Int) {
        val timeStamp : Long = getItem(position)
        holder.timeStamp = timeStamp
        holder.tvRingTime.text = activityContext.getString(
            R.string.rang_at, formatter.format(Date(timeStamp * 1000))
        )
        //Picasso.get().load(ref).into(holder.ivPhoto)
        Glide.with(activityContext)
            .load(StorageManager.getPhotoUrl(timeStamp))
            .into(holder.ivPhoto)
    }

    override fun getItemViewType(position: Int): Int = position % DIFFERENT_COLORS_NUMBER

    companion object {
        private const val DIFFERENT_COLORS_NUMBER = 3
        private const val VIEW_IS_NULL = "DoorbellRecordViewHolder.onClick view is null"
    }
}