package ua.com.user576.netsmartdoorbell.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ua.com.user576.netsmartdoorbell.databinding.ResolveRequestScreenBinding
import com.bumptech.glide.Glide
import ua.com.user576.netsmartdoorbell.Esp32
import ua.com.user576.netsmartdoorbell.LedState
import ua.com.user576.netsmartdoorbell.MainActivity
import ua.com.user576.netsmartdoorbell.RecordsManager
import ua.com.user576.netsmartdoorbell.StorageManager
import kotlin.concurrent.thread

class ResolveRequestFragment : Fragment() {

    private var binding : ResolveRequestScreenBinding? = null

/*    private val listener = object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
            val record : DoorbellRecord? = snapshot.getValue(DoorbellRecord::class.java)
            if (record == null) {
                Log.w(LOG_TAG, RECORD_IS_NULL_LOG)
            } else {
                val ref = FirebaseStorage.getInstance().getReferenceFromUrl(record.imageUrl)
                binding?.let {
                    GlideApp.with(this@ResolveRequestFragment)
                        .load(ref)
                        .into(it.photoImageView)
                }
            }
        }

        override fun onCancelled(error: DatabaseError) {
            Log.w(LOG_TAG, ON_CANCELLED_LOG, error.toException())
        }
    }*/

/*
    private lateinit var recordReference : DatabaseReference
    private lateinit var firebaseDb : FirebaseDB
*/

    private var timeStamp : Long = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val nonNullBinding = ResolveRequestScreenBinding.inflate(inflater, container, false)
        binding = nonNullBinding

        nonNullBinding.timeInstantLabel.text = arguments?.getString(TIME_INSTANT_READABLE_STRING_KEY)
        timeStamp = requireArguments().getLong(TIME_INSTANT_KEY)
        Glide.with(this)
            .load(StorageManager.getPhotoUrl(timeStamp))
            .into(nonNullBinding.photoImageView)
/*
        firebaseDb = FirebaseDB()
        recordReference = firebaseDb.getRecordAt(index)
        recordReference.addValueEventListener(listener)
*/

        nonNullBinding.approveBtn.setOnClickListener {
            approve()
        }
        nonNullBinding.denyBtn.setOnClickListener {
            deny()
        }

        return nonNullBinding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun closeFragment() {
        activity?.let {
            if (it is MainActivity) {
                it.toRequestsList()
            }
        }
    }

    private fun sendSignal(ledState: LedState) {
        thread {
            Esp32.setLed(ledState)
        }.join()
    }

    private fun removePhotoAndCloseFragment() {
        StorageManager.deletePhoto(timeStamp)
        RecordsManager.removeRecord(timeStamp)
        closeFragment()
    }

    private fun approve() {
        sendSignal(LedState.GREEN)
        removePhotoAndCloseFragment()
    }

    private fun deny() {
        sendSignal(LedState.RED)
        removePhotoAndCloseFragment()
    }

    companion object {
        internal const val TIME_INSTANT_READABLE_STRING_KEY = "time_instant_readable_string_key"
        internal const val TIME_INSTANT_KEY = "time_instant_key"
        private const val RECORD_IS_NULL_LOG = "ValueEventListener.onDataChange: record is null"
        private const val ON_CANCELLED_LOG = "onCancelled@ResolveRequestFragment"
    }
}