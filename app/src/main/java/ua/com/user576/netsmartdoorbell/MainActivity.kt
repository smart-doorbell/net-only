package ua.com.user576.netsmartdoorbell

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import dagger.hilt.android.AndroidEntryPoint
import ua.com.user576.netsmartdoorbell.ui.ForegroundStatusFragment
import ua.com.user576.netsmartdoorbell.ui.IpAddressFragment
import ua.com.user576.netsmartdoorbell.ui.RequestsListFragment
import ua.com.user576.netsmartdoorbell.ui.ResolveRequestFragment
import javax.inject.Inject
import kotlin.concurrent.thread

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var prefs: PrefsManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        StorageManager.photosFolder = filesDir.absolutePath
        val savedIp = prefs.getEsp32Address()
        if (savedIp == NET_MASK) {
            showIpAddressFragment()
        } else {
            Esp32.ipAddress = savedIp
        }
    }

    override fun onStart() {
        super.onStart()
        startService(Intent(applicationContext, TcpServerService::class.java))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        return true
    }

    fun toResolveRequestFragment(arguments : Bundle) {
        supportFragmentManager.commit {
            replace<ResolveRequestFragment>(R.id.fragment_container_view, args = arguments)
            addToBackStack(null)
        }
    }

    fun toRequestsList() {
        supportFragmentManager.commit {
            replace<RequestsListFragment>(R.id.fragment_container_view)
        }
    }

    private fun showIpAddressFragment() {
        IpAddressFragment().show(supportFragmentManager, "ip_address")
    }

    fun onOptionsMenuItemClick(menuItem: MenuItem) {
        when(menuItem.itemId) {
            R.id.ip_address -> {
                showIpAddressFragment()
            }
            R.id.foreground_service -> {
                println("Info about service should be displayed!")
                ForegroundStatusFragment().show(supportFragmentManager, "dialog")
            }
            R.id.live_stream -> {
                val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://${Esp32.ipAddress}:81/stream"))
                startActivity(myIntent)
            }
            R.id.exit -> {
                thread {
                    TcpServerService.stop()
                }
                finish()
            }
        }
    }
}