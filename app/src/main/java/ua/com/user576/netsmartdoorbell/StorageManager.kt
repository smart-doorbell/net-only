package ua.com.user576.netsmartdoorbell

import java.io.BufferedInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.URL

object StorageManager {
    private const val extension = ".jpg"
    var photosFolder = ""

    fun getPhotoNamesWithoutExtension() : List<String> {
        val photoFullNames = File(photosFolder).list() ?: return emptyList()
        return photoFullNames.map { nameWithExtension ->
            nameWithExtension.subSequence(0, nameWithExtension.length - extension.length).toString()
        }
    }

    private fun getAbsolutePathToPhoto(timeStamp : String) : String =
        "$photosFolder/$timeStamp$extension"

    private fun getAbsolutePathToPhoto(timeStamp : Long) : String =
        getAbsolutePathToPhoto(timeStamp.toString())


    fun getPhotoUrl(timeStamp : Long) : String {
        val absolutePath = getAbsolutePathToPhoto(timeStamp.toString())
        return "file://$absolutePath"
    }

    fun deletePhoto(timeStamp: Long) {
        File(getAbsolutePathToPhoto(timeStamp)).delete()
    }

    internal fun downloadAndSavePhoto(fileName : String) {
        val ip = Esp32.ipAddress
        val url = URL("http://$ip/capture")
        val inputStream: InputStream = BufferedInputStream(url.openStream())
        val out = ByteArrayOutputStream()
        val buf = ByteArray(1024)
        var n: Int
        while (-1 != inputStream.read(buf).also { n = it }) {
            out.write(buf, 0, n)
        }
        out.close()
        inputStream.close()
        val response: ByteArray = out.toByteArray()

        val fos = FileOutputStream(getAbsolutePathToPhoto(fileName))
        fos.write(response)
        fos.close()
    }
}